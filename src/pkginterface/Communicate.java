/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkginterface;

/**
 *
 * @author aluno.redes
 */
public interface Communicate {
    
    public void sendMessage(String Message);
    public String receivedMessage();
    
}
