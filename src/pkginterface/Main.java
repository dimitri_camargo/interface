/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkginterface;

import java.util.Scanner;
import javax.swing.JFrame;

/**
 *
 * @author shewremamaral
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        
//        Person obj = new Person();
//        obj.setName("João");
        
        //object casting
        MyInterface obj = new Person();
        ((Person)obj).setName("João");
        
//        Form desktop = new Form();
        MyInterface desktop = new Form();
        //showing the JFrame
        ((JFrame)desktop).setVisible(true);
        
        System.out.println("digit message to send to objects: ");
        String msg = read.nextLine();
        
        //invoking methods from interface
        obj.showMessage(msg);
        desktop.showMessage(msg);
        
        System.out.println("obj user: "+obj.getUser());
        System.out.println("desktop user: "+obj.getUser());
    }
    
}
